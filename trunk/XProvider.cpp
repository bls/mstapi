/*
 *  XTAPI JTapi implementation
 *  Copyright (C) 2002 Steven A. Frare
 * 
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * @author  Steven A. Frare
 * @version .01
 
 The original files were lastly changed in April 2021 by Fabian Hall - f.hall (at) bls-integration.de
 */

#define TAPI_MIN_SUPPORTED 0x00010004
#define TAPI_MAX_SUPPORTED 0x00020002

#include "Log.h"
#include <map>
#include <memory>
#include <process.h>
#include "XTapi.h"

/**
 * Gets the name and address from a call info
 * 
 * @param callInfo reference to a struct containing call information
 * @param name reference to the string to which to write the name from the call information
 * @param address reference to the string to which to write the address from the call information
 */
void getCallPartyInformation(const LINECALLINFO& callInfo, std::string& name, std::string& address);

/**
 * Initializes the TAPI and retrievs the amount of lines which can be used by this application. It
 * is possible that not all existing lines may be available for this application. To determine
 * which lines can be used, @c lineOpen has to be called.
 * 
 * @param dwNumLines the pointer to which the amount of usable TAPI lines will be written
 * @return 0 if the lines could be initialized, otherwise the error returned by lineInitialize
 */
LONG InitTapi(DWORD * dwNumLines);

/**
 * Prints all of the device's known addresses to the log
 *
 * @param deviceId the id of the device to be queried
 */
void logAllAddresses(jint deviceId);

/**
 * Reads the address belonging to the specified address ID of the given line
 *
 * @param lLine the ID of the queried line
 * @param addressID the address's ID
 * @return the address belonging to the given ID and device
 */
std::string readAddress(jint lLine, DWORD addressId);

/**
 * Reads the address belonging to address ID 0 of the given line
 *
 * @param lLine the ID of the queried line
 * @return the device's address with the ID 0
 */
std::string readFirstAddress(jint lLine);

/**
 * Works the message queue. This function won't return until the application shuts down.
 */
void WINAPI workMessageQueue();

/**
 * pointer to JNI function pointers
 */
JNIEnv * g_env = nullptr;
/**
 * the object which initialized the TAPI
 */
jobject g_thisObj = nullptr;

/**
 * Instance handle of this very DLL
 */
HINSTANCE g_hinstDLL;
/**
 * Handle to destination window
 */
HWND g_hWnd;
/**
 * the application's usage handle for the TAPI
 */
HLINEAPP g_hTAPI;
/**
 * the negotiated TAPI version for each line device
 */
std::map<jint, DWORD> negotiatedTapiVersions;
/**
 * Time in ms to wait before failing on placing a call
 */
const int MAXCALLWAIT_MS = 5000;
/**
 * Message constant for requesting to shut down
 */
const UINT SHUTDOWN_MESSAGE = WM_USER + 1;

LONG LineCallStateProc(DWORD dwDevice, DWORD_PTR dwInstance, DWORD_PTR dwParam1,
		DWORD_PTR dwParam2, DWORD_PTR dwParam3)
{
	LONG error;
	switch (dwParam1)
	{
		case LINECALLSTATE_IDLE:
			error = lineDeallocateCall((HCALL) dwDevice);
			LOG_WARN_ON_ERROR_CODE(error, "lineDeallocateCall returned error code %#08x", error);
			break;
		default:
			break;
	}
	return 0;
}

/**
 * MS TAPI call back function for line events
 *
 * @param dwDevice A handle to either a line device or a call associated with the callback.
 * @param dwMessage A line or call device message.
 * @param dwInstance Callback instance data passed back to the application in the callback.
 * @param dwParam1 A parameter for the message.
 * @param dwParam2 A parameter for the message.
 * @param dwParam3 A parameter for the message.
 */
void CALLBACK LineCallBackProc(DWORD dwDevice, DWORD dwMessage, const DWORD_PTR dwInstance,
		DWORD_PTR dwParam1, DWORD_PTR dwParam2, DWORD_PTR dwParam3)
{
	// Pre-Process the event to see if we to do any work on it before publishing
	switch (dwMessage)
	{
		case LINE_CALLSTATE:
			LineCallStateProc(dwDevice, dwInstance, dwParam1, dwParam2, dwParam3);
			break;
		default:
			break;
	}

	// Publish this event into the IXTapi Object
	jclass mstapiClass = g_env->GetObjectClass(g_thisObj);
	const char * methodName = "callback";
	jmethodID callbackMethod = g_env->GetMethodID(mstapiClass, methodName, "(IIIIII)V");
	if (callbackMethod == nullptr)
	{
		LOG_FATAL("Could not find Java callback method '%s'", methodName);
		return;
	}
	LOG_DEBUG("Called the callback method with dwMessage: %d", dwMessage);
	g_env->CallVoidMethod(g_thisObj, callbackMethod, dwDevice, dwMessage, dwInstance, dwParam1, dwParam2, dwParam3);
	if (g_env->ExceptionCheck())
	{
		LOG_ERROR("Exception when calling Java callback method");
		return;
	}
}

LONG InitTapi(DWORD * dwNumLines)
{
	LOG_TRACE("Entered InitTapi");

	g_hTAPI = 0;
	LPCSTR szApplicationName = "JONYX_TAPI";
	DWORD tapiVersion = TAPI_MAX_SUPPORTED;
	LINEINITIALIZEEXPARAMS initParams;
	initParams.dwTotalSize = sizeof(initParams);
	initParams.dwOptions = LINEINITIALIZEEXOPTION_USEHIDDENWINDOW; // same mechanism as TAPI 1.4
	LONG error = lineInitializeEx(&g_hTAPI, g_hinstDLL, LineCallBackProc, szApplicationName,
			dwNumLines, &tapiVersion, &initParams);
	if (error == 0)
	{
		LOG_INFO("lineInitializeEx found %u lines. The TAPI supports version %#x", *dwNumLines, tapiVersion);
	}
	else
	{
		LOG_ERROR("lineInitializeEx returned error code %#08x for TAPI version %#x", error, tapiVersion);
	}

	LOG_TRACE("Left InitTapi");
	return error;
}

/**
 * Inits MS TAPI, sets the m_numLines field in the IXTapi object to the number of lines to use
 * (NOTE: Only lines which this application can open can be used. This function will not open
 * any lines itself.)
 *
 * This thread stays in this .dll to run the message pump. It will return on shutdown or if the
 * initialization of the TAPI failes.
 * 
 * @param pEnv pointer to JNI function pointers
 * @param oObj the object calling this function
 * @return 0 if successful and the initialization's error code in case of failure.
 */
JNIEXPORT jint JNICALL Java_net_xtapi_serviceProvider_MSTAPI_initTapi(JNIEnv * pEnv, jobject oObj)
{
	LOG_TRACE("Entered TAPI initialization");

	// store information about the initiator
	g_thisObj = oObj;
	g_env = pEnv;

	DWORD dwNumLines;
	LONG error = InitTapi(&dwNumLines);
	if (error < 0)
	{
		LOG_TRACE("Left TAPI initialization with error %#08x", error);
		return error;
	}
	
	const char * attributeName = "m_numLines";
	LOG_DEBUG("Try to set caller field %s to %lu", attributeName, dwNumLines);
	jclass mstapiClass = g_env->GetObjectClass(g_thisObj);
	jfieldID numLinesAttribute = g_env->GetFieldID(mstapiClass, attributeName, "I");
	if (numLinesAttribute == nullptr)
	{
		LOG_ERROR("The calling class has no fitting attribute '%s'", attributeName);
		LOG_TRACE("Left TAPI initialization with error -1");
		return -1;
	}
	g_env->SetIntField(g_thisObj, numLinesAttribute, dwNumLines);
	LOG_DEBUG("Successfully set caller field %s to %lu", attributeName, dwNumLines);

	initSound(dwNumLines);
	workMessageQueue();

	LOG_TRACE("Left TAPI initialization");
	return 0;
}

/**
 * Sets the log level which can be used for debugging
 * 
 * @param pEnv pointer to JNI function pointers
 * @param oObj the object calling this function
 * @param logLevel the log level to use from now on
 */
JNIEXPORT void JNICALL Java_net_xtapi_serviceProvider_MSTAPI_debug(JNIEnv * pEnv, jobject oObj, jint logLevel)
{
	int level = (int) logLevel;
	for (int i = 0; i < LOG_LEVEL.numLevels; ++i)
	{
		const Severity severity = LOG_LEVEL.levels[i];
		if (level == severity.value)
		{
			LOG_INFO("Changed minimum log level to %s", severity.name);
			setMinSeverity(severity);
			return;
		}
	}
	LOG_WARN("Could not change the log level to %d as there is no such log level", level);
}

/**
 * Opens a single MS TAPI line.
 * 
 * @param pEnv pointer to JNI function pointers
 * @param oObj the object calling this function
 * @param lLine the ID of the line to be opened
 * @return the TAPI line handle if succesful, otherwise a negative integer
 */
JNIEXPORT jint JNICALL Java_net_xtapi_serviceProvider_MSTAPI_openLine(JNIEnv * pEnv, jobject oObj, jint lLine)
{
	LINEEXTENSIONID lpExtensionID;
	DWORD negotiatedVersion;
	LONG error = lineNegotiateAPIVersion(g_hTAPI, lLine, TAPI_MIN_SUPPORTED, TAPI_MAX_SUPPORTED,
			&negotiatedVersion, &lpExtensionID);
	if (error != 0)
	{
		LOG_ERROR("Failed to negotiate a TAPI version (between %#x and %#x) for line %d.",
				TAPI_MIN_SUPPORTED, TAPI_MAX_SUPPORTED, lLine);
		return -1;
	}
	negotiatedTapiVersions[lLine] = negotiatedVersion;
	LOG_INFO("Negotiated TAPI version %#x for line %d.", negotiatedVersion, lLine);

	HLINE hLine;
	error = lineOpen(g_hTAPI, lLine, &hLine, negotiatedVersion, 0, lLine, LINECALLPRIVILEGE_MONITOR, 0, 0);
	LOG_ERROR_ON_ERROR_CODE(error, "lineOpen on line %d returned error code %#08x", lLine, error);
	if (error != 0)
	{
		return error;
	}

	logAllAddresses(lLine);
	return (long) hLine;
}

JNIEXPORT jobject JNICALL Java_net_xtapi_serviceProvider_MSTAPI_getLineInfo(JNIEnv * pEnv, jobject oObj, jint lLine)
{
	int devCapsSize = sizeof(LINEDEVCAPS) + 1024;
	std::unique_ptr<LINEDEVCAPS> pDevCaps((LINEDEVCAPS *) ::operator new(devCapsSize));
	memset(pDevCaps.get(), 0, devCapsSize);
	pDevCaps->dwTotalSize = devCapsSize;
	
	DWORD tapiVersion = negotiatedTapiVersions[lLine];
	LONG error = lineGetDevCaps(g_hTAPI, lLine, tapiVersion, 0, pDevCaps.get());
	LOG_ERROR_ON_ERROR_CODE(error, "lineGetDevCaps for line %d returned error code %#08x", lLine, error);
	if (error != 0)
	{
		return nullptr;
	}

	std::string firstAddress;
	if (pDevCaps->dwNumAddresses > 0)
	{
		firstAddress = readFirstAddress(lLine);
	}
	std::string lineName = getTapiString(pDevCaps.get(), pDevCaps->dwLineNameOffset, pDevCaps->dwLineNameSize);
	std::string provider = getTapiString(pDevCaps.get(), pDevCaps->dwProviderInfoOffset, pDevCaps->dwProviderInfoSize);
	
	const char * className = "net/xtapi/serviceProvider/XTapiLineInfo";
	jclass infoClass = pEnv->FindClass(className);
	if (infoClass == nullptr)
	{
		LOG_ERROR("Could not find class '%s'", className);
		return nullptr;
	}
	jmethodID  infoConstructor = pEnv->GetMethodID(infoClass, "<init>", "(Ljava/lang/String;ILjava/lang/String;)V");
	if (infoConstructor == nullptr)
	{
		LOG_ERROR("Could not find constructor for class '%s'", className);
		return nullptr;
	}
	jobject infoObject = pEnv->NewObject(infoClass, infoConstructor, pEnv->NewStringUTF(lineName.c_str()),
			pDevCaps->dwPermanentLineID, pEnv->NewStringUTF(firstAddress.c_str()));
	if (infoObject == nullptr)
	{
		LOG_ERROR("Could not construct new object for class '%s'", className);
		return nullptr;
	}
	return infoObject;
}

std::string readFirstAddress(jint lLine)
{
	return readAddress(lLine, 0);
}

std::string readAddress(jint lLine, DWORD addressId)
{
	int addressCapsSize = sizeof(LINEADDRESSCAPS) + 1024;
	std::unique_ptr<LINEADDRESSCAPS> pAddressCaps((LINEADDRESSCAPS *) ::operator new(addressCapsSize));
	memset(pAddressCaps.get(), 0, addressCapsSize);
	pAddressCaps->dwTotalSize = addressCapsSize;

	DWORD tapiVersion = negotiatedTapiVersions[lLine];
	LONG error = lineGetAddressCaps(g_hTAPI, lLine, addressId, tapiVersion, 0, pAddressCaps.get());
	LOG_WARN_ON_ERROR_CODE(error, "lineGetAddressCaps for line %d returned error code %#08x", lLine, error);
	if (error != 0)
	{
		return nullptr;
	}
	return getTapiString(pAddressCaps.get(), pAddressCaps->dwAddressOffset, pAddressCaps->dwAddressSize);
}

/**
 * Establish a call to a destination address from an open TAPI line.
 * 
 * @param pEnv pointer to JNI function pointers
 * @param oObj the object calling this function
 * @param lLine line number (for debugging)
 * @param oDest destination address
 * @param lHandle line handle
 * @return the handle of the created call if succesful, otherwise -1
 */
JNIEXPORT jint JNICALL Java_net_xtapi_serviceProvider_MSTAPI_connectCall(JNIEnv * pEnv,
		jobject oObj, jint lLine, jstring oDest, jint lHandle)
{
	jboolean isCopy;
	const char * utf_string = pEnv->GetStringUTFChars(oDest, &isCopy);

	LOG_INFO("Will place call from line %d to %s.", lLine, utf_string);

	HCALL hCall = 0;
	LONG result = lineMakeCall((HLINE) lHandle, &hCall, utf_string, 0, nullptr);
	if (JNI_TRUE == isCopy)
	{
		pEnv->ReleaseStringUTFChars(oDest, utf_string);
	}
	if (result <= 0)
	{
		LOG_ERROR("lineMakeCall returned error code %#08x", result);
		return -1;
	}
	else
	{
		LOG_DEBUG("lineMakeCall returned request identifier %d", result);
	}
	
	// Wait for a change of hCall which signals that the async call to lineMakeCall finished
	DWORD sleepDuration = 20;
	int passedTime = 0;
	for (; (long) hCall == 0 && passedTime <= MAXCALLWAIT_MS; passedTime += sleepDuration)
	{
		Sleep(sleepDuration);
	}

	LOG_DEBUG("Waited %d milliseconds for lineMakeCall", passedTime);
	LOG_INFO("lineMakeCall reserved the call handle %d", (long) hCall);

	return (long) hCall;
}

/**
 * Accept an incoming TAPI call.
 * 
 * @param pEnv pointer to JNI function pointers
 * @param oObj the object calling this function
 * @param lHandle the handle of the call to be answered
 * @return a positive identifier for this call or a negative integer in case of failure.
 */
JNIEXPORT jint JNICALL Java_net_xtapi_serviceProvider_MSTAPI_answerCall(JNIEnv * pEnv, jobject oObj, jint lHandle)
{
	LOG_INFO("answer call handle %d", lHandle);

	LONG error = lineSetCallPrivilege((HCALL) lHandle, LINECALLPRIVILEGE_OWNER);
	LOG_ERROR_ON_ERROR_CODE(error, "lineSetCallPrivilege with privilege OWNER returned error code %#08x", error);
	if (error != 0)
	{
		return error;
	}

	LONG result = lineAnswer((HCALL) lHandle, nullptr, 0);
	if (result <= 0)
	{
		LOG_WARN("lineAnswer returned error code %#08x", result);
	}
	else
	{
		LOG_DEBUG("lineAnswer returned request identifier %d", result);
	}

	return result;
}

/**
 * Drops a call.
 * This may affect related calls, e. g. if it is a conference call, all other participating calls
 * might be dropped as well.
 * 
 * @param pEnv pointer to JNI function pointers
 * @param oObj the object calling this function
 * @param lHandle the handle of the 
 * @return 0 on success, otherwise the negative identifier for the error code
 */
JNIEXPORT jint JNICALL Java_net_xtapi_serviceProvider_MSTAPI_dropCall(JNIEnv * pEnv, jobject oObj, jint lHandle)
{
	LOG_INFO("dropCall dropping call handle %d", lHandle);

	LONG result = lineDrop((HCALL) lHandle, nullptr, 0);
	if (result <= 0)
	{
		LOG_WARN("lineDrop returned error code %#08x", result);
		return result;
	}
	
	LOG_DEBUG("lineDrop returned request identifier %d", result);
	return 0;
}

/**
 * Set whether or not to send digit events from MS TAPI.
 * 
 * @param pEnv pointer to JNI function pointers
 * @param oObj the object calling this function
 * @param lHandle call handle for which the event handling is set
 * @param enable true if send digit events are to be send, false otherwise
 * @return 0 on success or a negative integer in case of failure.
 */
JNIEXPORT jint JNICALL Java_net_xtapi_serviceProvider_MSTAPI_monitorDigits(JNIEnv * pEnv,
		jobject oObj, jint lHandle, jboolean enable)
{
	DWORD digitModes = enable ? LINEDIGITMODE_DTMF : 0;
	return lineMonitorDigits((HCALL) lHandle, digitModes);
}

/**
 * Gets caller id information
 * 
 * @param pEnv pointer to JNI function pointers
 * @param oObj the object calling this function
 * @param lHandle the call handle
 * @return a String[] of length 2 with s[0] = caller's name and s[1] = caller address
 */
JNIEXPORT jobjectArray JNICALL Java_net_xtapi_serviceProvider_MSTAPI_getCallInfo(JNIEnv * pEnv,
		jobject oObj, jint lHandle)
{
	long callInfoSize = sizeof(LINECALLINFO) + 1024;
	std::unique_ptr<LINECALLINFO> lpCallInfo((LINECALLINFO *) ::operator new(callInfoSize));
	lpCallInfo->dwTotalSize = callInfoSize;
	
	LONG error = lineGetCallInfo((HCALL) lHandle, lpCallInfo.get());
	LOG_WARN_ON_ERROR_CODE(error, "lineGetCallInfo returned error code %#08x", error);
	if (error != 0)
	{
		return nullptr;
	}

	std::string name;
	std::string address;
	getCallPartyInformation(*lpCallInfo, name, address);
	
	const char * className = "java/lang/String";
	jclass stringClass = pEnv->FindClass(className);
	if (stringClass == nullptr)
	{
		LOG_ERROR("Could not find class '%s'", className);
		return nullptr;
	}
	// Create a two element string array to hold the Name and Address
	jobjectArray callInfo = pEnv->NewObjectArray(2, stringClass, nullptr);
	pEnv->SetObjectArrayElement(callInfo, 0, pEnv->NewStringUTF(name.c_str()));
	if (pEnv->ExceptionCheck())
	{
		LOG_ERROR("Could not fill call info with call party name");
		return nullptr;
	}
	pEnv->SetObjectArrayElement(callInfo, 1, pEnv->NewStringUTF(address.c_str()));
	if (pEnv->ExceptionCheck())
	{
		LOG_ERROR("Could not fill call info with call party address");
		return nullptr;
	}
	return callInfo;
}

void getCallPartyInformation(const LINECALLINFO& callInfo, std::string& name, std::string& address)
{
	DWORD partyIDFlags;
	DWORD partyIDSize;
	DWORD partyIDOffset;
	DWORD partyIDNameSize;
	DWORD partyIDNameOffset;
	if (callInfo.dwOrigin == LINECALLORIGIN_OUTBOUND)
	{
		partyIDFlags = callInfo.dwCalledIDFlags;
		partyIDSize = callInfo.dwCalledIDSize;
		partyIDOffset = callInfo.dwCalledIDOffset;
		partyIDNameSize = callInfo.dwCalledIDNameSize;
		partyIDNameOffset = callInfo.dwCalledIDNameOffset;
	}
	else
	{
		partyIDFlags = callInfo.dwCallerIDFlags;
		partyIDSize = callInfo.dwCallerIDSize;
		partyIDOffset = callInfo.dwCallerIDOffset;
		partyIDNameSize = callInfo.dwCallerIDNameSize;
		partyIDNameOffset = callInfo.dwCallerIDNameOffset;
	}

	if ((partyIDFlags & LINECALLPARTYID_OUTOFAREA) != 0)
	{
		name = "OUTOFAREA";
		address = "OUTOFAREA";
	}
	else if ((partyIDFlags & LINECALLPARTYID_BLOCKED) != 0)
	{
		name = "BLOCKED";
		address = "BLOCKED";
	}
	else
	{
		bool hasName = (partyIDFlags & LINECALLPARTYID_NAME) != 0;
		name = hasName ? getTapiString(&callInfo, partyIDNameOffset, partyIDNameSize) : "UNKNOWN";
		bool hasAddress = (partyIDFlags & LINECALLPARTYID_ADDRESS) != 0;
		address = hasAddress ? getTapiString(&callInfo, partyIDOffset, partyIDSize) : "UNKNOWN";
	}
}

/**
 * Asynchronously send a string of digits as DTMF tones.
 * 
 * @param pEnv pointer to JNI function pointers
 * @param oObj the object calling this function
 * @param lHandle call handle
 * @param oDigits the digits to be send as DTMF tones
 * @return zero for success or any other integer for failure.
 */
JNIEXPORT jint JNICALL Java_net_xtapi_serviceProvider_MSTAPI_sendDigits(JNIEnv * pEnv,
		jobject oObj, jint lHandle, jstring oDigits)
{
	jboolean isCopy;
	const char * utf_string = pEnv->GetStringUTFChars(oDigits, &isCopy);
	LONG error = lineGenerateDigits((HCALL) lHandle, LINEDIGITMODE_DTMF, utf_string, 0);
	if (JNI_TRUE == isCopy)
	{
		pEnv->ReleaseStringUTFChars(oDigits, utf_string);
	}
	LOG_WARN_ON_ERROR_CODE(error, "lineGenerateDigits returned error code %#08x", error);

	return error;
}

/**
 * Shuts down the secondary init thread.
 * 
 * @param pEnv pointer to JNI function pointers
 * @param oObj the object calling this function
 * @return jint If the function succeeds, the return value is nonzero.
 */
JNIEXPORT jint JNICALL Java_net_xtapi_serviceProvider_MSTAPI_shutDown(JNIEnv * pEnv, jobject oObj)
{
	return PostMessage(g_hWnd, SHUTDOWN_MESSAGE, 0, 0);
}

void WINAPI workMessageQueue()
{
	LOG_INFO("Start working the message queue");
	MSG msg;
	while (GetMessage(&msg, nullptr, 0, 0))
	{
		g_hWnd = msg.hwnd;
		if (msg.message == SHUTDOWN_MESSAGE)
		{
			break;
		}
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	LOG_INFO("Stop working the message queue");

	if (g_hTAPI != 0)
	{
		LONG error = lineShutdown(g_hTAPI);
		g_hTAPI = 0;
		LOG_WARN_ON_ERROR_CODE(error, "lineShutdown returned error code %#08x", error);
	}
}

std::string getTapiString(const void * ptr, DWORD offset, DWORD size)
{   
	return std::string((char *) ((uintptr_t) ptr + offset), size);
}

void logAllAddresses(jint deviceId)
{
	long devCapsSize = sizeof(LINEDEVCAPS) + 1024;
	std::unique_ptr<LINEDEVCAPS> lpDevCaps((LINEDEVCAPS *) ::operator new(devCapsSize));
	lpDevCaps->dwTotalSize = devCapsSize;

	DWORD tapiVersion = negotiatedTapiVersions[deviceId];
	LONG error = lineGetDevCaps(g_hTAPI, deviceId, tapiVersion, 0, lpDevCaps.get());
	LOG_WARN_ON_ERROR_CODE(error, "lineGetDevCaps returned error code %#08x", error);
	if (error != 0)
	{
		return;
	}

	DWORD numAddresses = lpDevCaps->dwNumAddresses;
	LOG_DEBUG("lineGetDevCaps knows about %lu address(es) for line %lu", numAddresses, deviceId);
	for (DWORD i = 0; i < numAddresses; ++i)
	{
		std::string address = readAddress(deviceId, i);
		LOG_INFO("readAddress retrieved address %s from line %lu", address.c_str(), deviceId);
	}
}

extern "C"
{
	/**
	 * Starts the log on startup and closes it and the sound processing on shutdown.
	 * This function is automatically called by the process which loads or terminates this DLL, e. g.
	 * when using LoadLibrary and FreeLibrary functions.
	 * The return value is only used by the calling process if @p is @c DLL_PROCESS_ATTACH.
	 * 
	 * @param hinstDLL the DLL's handle which is its base address
	 * @param fdwReason reason describing why this entry-point has been called
	 * @param lpvReserved A pointer describing the startup or shutdown circumstances - may be NULL
	 * @return @c TRUE
	 */
	BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
	{
		if (fdwReason == DLL_PROCESS_ATTACH)
		{
			openLog();
			LOG_DEBUG("DLL_PROCESS_ATTACH");
			g_hinstDLL = (HINSTANCE) hinstDLL;
		}
		else if (fdwReason == DLL_PROCESS_DETACH)
		{
			LOG_DEBUG("DLL_PROCESS_DETACH");
			teardownSound();
			closeLog();
		}
		return TRUE;
	}
}
