/*
*  XTAPI JTapi implementation
*  Copyright (C) 2002 Steven A. Frare
* 
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*  
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*  
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
 * @author  Steven A. Frare
 * @version .01
 
 The original files were lastly changed in April 2021 by Fabian Hall - f.hall (at) bls-integration.de
 */

#include <windows.h>
#define TAPI_CURRENT_VERSION 0x00020002
#include <tapi.h>
#include <stdio.h>
#include "net_xtapi_ServiceProvider_MSTAPI.h"
#include <string>

extern void initSound(int);
extern void teardownSound();

/**
 * Get a string from a TAPI structure
 *
 * @param ptr pointer to the structure from which to read the string
 * @param offset offset from the pointer after which the string starts
 * @param size the size of the string to read
 * @return the read string
 */
std::string getTapiString(const void * ptr, DWORD offset, DWORD size);
