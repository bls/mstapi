/*
*  XTAPI JTapi implementation
*  Copyright (C) 2002 Steven A. Frare
* 
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*  
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*  
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
 * @author  Steven A. Frare
 * @version .01

 The original files were lastly changed in April 2021 by Fabian Hall - f.hall (at) bls-integration.de
 */

#include "XTapi.h"
#include "Log.h"

//Wave format is CCITT u-Law 8.000 kHz, 8 Bit, Mono CODEC
//per Microsoft Knowledge Base Article ID: Q142745
WAVEFORMATEX messageFormat = {7, 1, 8000, 8000, 1, 8, 0};

// Number of buffers for recording, keep it an even number it is divided by 
// two later on.  Our buffers can get very small, 100ms so even at 20 buffers
// which we begin half way through we only have about a second of buffer
// in the worst case scenario.
const int NUM_BUFFERS = 20;

// TAPI speak for playing / recording devices as mapped to the TAPI device.
const char * waveOut= "wave/out";	
const char * waveIn	= "wave/in";

// Internal function prototypes, see implementation for details
HWAVEOUT playSound (DWORD dw_devid, char *szWaveFile);
HWAVEIN	recordMessage (DWORD dw_devid, const char * filename, HWAVEOUT hWaveOut, DWORD out_id);
long	waveAlloc(HWAVEIN * h_wavein, WAVEHDR * waveHeader, int divisor);
HWAVEOUT openPlayDevice(DWORD device);
int		saveMessage (char *name, LPWAVEHDR hdr, unsigned long ulBytes, FILE *fptr);
int		getDivisor();

// This struct tracks the state of our wave devices.
typedef struct sound
{
	bool b_playData;	// finished playing a segment of data
	bool b_recDone;		// Stop recording
	bool b_recData;		// got a segment of data
	bool b_play_Rec;	// this device is busy playing or recording
	bool b_useDefault;	// We are using the default line (mic or speakers)
	WAVEHDR * recHeader;// header to the last recording data we got
	WAVEHDR * playHeader;// header to the last playing data we got
} t_sound;

t_sound * m_devices = nullptr;

/**
 * Initialize the m_devices array to track the state of all wave devices
 * that may potentially be used.
 * 
 * @param num_device the number of lines initialized by the TAPI
 */
void initSound(int num_devices)
{
	// Over allocate by one since we use the wave id of the device
	// and for useDefaultSpeaker and useDefaultMicrophone that id is the
	// Microsoft MAPPER device which is -1 so we always use (device_id + 1).
	// If we have a full-duplex wave device this may need to be twice as 
	// large so go ahead and allocate for that.
	m_devices = new t_sound[(num_devices * 2) + 1];
}

/**
 * Release any allocated resources.
 */
void teardownSound()
{
	delete [] m_devices;
	m_devices = nullptr;
}

/**
 * Helper function to get the Wave device ID that correlates to the TAPI line.
 * 
 * @param hLine handle to the TAPI line
 * @param device one of the constants #waveOut or #waveIn
 */
long getWaveID(HLINE hLine, const char * device)
{
	// allocate memory for structure
	VARSTRING * vs = (VARSTRING *) calloc (1, sizeof(VARSTRING));
	// set structure size
	vs->dwTotalSize = sizeof(VARSTRING);
	do
	{
		// get information into structure
		LONG error = lineGetID(hLine, 0L, 0, LINECALLSELECT_LINE, vs, device);
		if (error)
		{
			LOG_ERROR("lineGetID returned error code %#08x", error);
			free(vs);
			return -1;
		}
		// try again if more space is needed
		if (vs->dwTotalSize < vs->dwNeededSize)
		{
			DWORD dwSize = vs->dwNeededSize;
			free(vs);
			vs = (VARSTRING *) calloc(1, dwSize);
			vs->dwTotalSize = dwSize;
			continue;
		}
		break; /* success  */
	} while (TRUE);
	
	// copy wave id
	DWORD dwWaveDev = (DWORD) *((DWORD *)((LPSTR)vs + vs->dwStringOffset));
	free(vs);
	
	LOG_DEBUG("Device %s has wave ID %d", device, dwWaveDev);
	
	return dwWaveDev;
}

/**
 * Record sound to a file or render the sound to the speakers.
 * If @p oFileName is NULL , the sound is rendered straight to the speakers and not recorded. There is no facility for
 * doing both at the same time.
 * 
 * @param JNIEnv pointer to the JNI Environment
 * @param oObj the Java object that called down the JNI layer
 * @param oFileName the file to save the recording to - may be NULL
 * @param lLine the line handle
 */
JNIEXPORT jint JNICALL Java_net_xtapi_serviceProvider_MSTAPI_recordSound(JNIEnv * pEnv,
		jobject oObj, jstring oFileName, jint lLine)
{
	jboolean isCopy;
	const char * szFile = nullptr;
	if (oFileName != nullptr)
	{
		szFile = pEnv->GetStringUTFChars(oFileName, &isCopy);
	}

	long id = (lLine != -1) ? getWaveID((HLINE) lLine, waveIn) : -1;
	m_devices[id + 1].b_play_Rec = true;
	m_devices[id + 1].b_useDefault = false;

	HWAVEOUT hWaveOut = nullptr;
	if (szFile == nullptr)
	{
		hWaveOut = openPlayDevice(-1);

		if (hWaveOut == nullptr)
		{
			m_devices[id + 1].b_play_Rec = false;
			m_devices[id + 1].b_useDefault = false;
			return 0;
		}
		m_devices[id + 1].b_useDefault = true;
	}

	long lRc = (long)recordMessage(id,szFile,hWaveOut, -1);

	if (JNI_TRUE == isCopy)
	{
		pEnv->ReleaseStringUTFChars(oFileName,szFile);
	}

	if (hWaveOut != nullptr)
	{
		lRc = waveOutReset(hWaveOut);
		if (lRc != MMSYSERR_NOERROR)
		{
			LOG_ERROR("waveOutReset error %d", lRc);
		}
		lRc = waveOutClose(hWaveOut);
		if (lRc != MMSYSERR_NOERROR)
		{
			LOG_ERROR("waveOutClose error %d", lRc);
		}
	}

	m_devices[id + 1].b_play_Rec = false;
	m_devices[id + 1].b_useDefault = false;

	return 1;
}

/**
 * Play sound from a file or use the default microphone as the source.
 * If sound is rendered from the microphone, then @p oFileName has to be NULL. In that case the sound from the
 * microphone is streamed through the TAPI.
 * 
 * @paramJNIEnv pointer to the JNI Environment
 * @param oObj the Java object that called down the JNI layer
 * @param oFileName the file to save the recording to - may be NULL
 * @param lLine the line handle
 */
JNIEXPORT jint JNICALL Java_net_xtapi_serviceProvider_MSTAPI_playSound(JNIEnv * pEnv, jobject oObj,
		jstring oFileName, jint lLine)
{
	long id = (lLine != -1) ? getWaveID((HLINE) lLine, waveOut) : -1;
	m_devices[id + 1].b_useDefault = false;
	m_devices[id + 1].b_play_Rec = true;

	const char * szFile = nullptr;
	jboolean isCopy;
	long lRc = 0;
	if (oFileName != nullptr)
	{
		szFile = pEnv->GetStringUTFChars(oFileName, &isCopy);
		lRc = (long)playSound(id,(char *)szFile);
	}
	else
	{
		HWAVEOUT hWaveOut = openPlayDevice(id);
		if (hWaveOut == nullptr)
		{
			m_devices[id + 1].b_play_Rec = false;
			m_devices[id + 1].b_useDefault = false;
			return 0;
		}

		m_devices[id + 1].b_useDefault = true;

		lRc = (long)recordMessage(-1, szFile, hWaveOut, id);

		if (hWaveOut != nullptr)
		{
			lRc = waveOutReset(hWaveOut);
			if(lRc != MMSYSERR_NOERROR)
			{
				LOG_ERROR("waveOutReset error %d",lRc);
			}

			lRc = waveOutClose(hWaveOut);
			if(lRc != MMSYSERR_NOERROR)
			{
				LOG_ERROR("waveOutClose error %d",lRc);
			}
		}

	}

	if (JNI_TRUE == isCopy)
	{
		pEnv->ReleaseStringUTFChars(oFileName,szFile);
	}

	m_devices[id + 1].b_play_Rec = false;
	m_devices[id + 1].b_useDefault = false;

	return lRc;
}

void CALLBACK waveOutProc(HWAVEOUT hwo, UINT uMsg, DWORD dwInstance,  DWORD dwParam1, DWORD dwParam2)
{	
	if (uMsg == WOM_DONE)
	{
		WAVEHDR * waveHeader = (WAVEHDR *) dwParam1;
		m_devices[waveHeader->dwUser + 1].b_playData = true;
		m_devices[waveHeader->dwUser + 1].playHeader = (WAVEHDR*)dwParam1;

	}
}

void CALLBACK waveInProc(HWAVEIN hwi, UINT uMsg, DWORD dwInstance,  DWORD dwParam1, DWORD dwParam2)
{
	if (uMsg == WIM_DATA)
	{
		WAVEHDR * waveHeader = (WAVEHDR *) dwParam1;
		m_devices[waveHeader->dwUser + 1].b_recData = true;
		m_devices[waveHeader->dwUser + 1].recHeader = (WAVEHDR*)dwParam1;
	}
}

/**
 * Plays the wave file out of the device
 * 
 * @param dw_deviceid the ID of the device to play the sound
 * @param szWaveFile path to a wave file (preferably CCITT u-Law 8kHz 8 bit Mono encoded)
 * @return
 */
HWAVEOUT playSound(DWORD dw_devid, char * szWaveFile)
{
	LOG_TRACE("Entered playSound");
	
	// Open wave file 
	HMMIO hmmio = mmioOpen(szWaveFile, nullptr, MMIO_READ | MMIO_ALLOCBUF);
	if (!hmmio)
	{
		LOG_ERROR("Failed on mmioOpen");
		return 0;
	}
	
	// Locate a 'RIFF' chunk with a 'WAVE' form type 
	MMCKINFO mmckinfoParent;
	mmckinfoParent.fccType = mmioFOURCC('W', 'A', 'V', 'E');
	if (mmioDescend(hmmio, (LPMMCKINFO) &mmckinfoParent, nullptr, MMIO_FINDRIFF))
	{
		LOG_ERROR("Failed to find the WAVE form type.");
		mmioClose(hmmio, 0);
		return 0;
	}
	
	// Find the format chunk
	MMCKINFO mmckinfoSubchunk;
	mmckinfoSubchunk.ckid = mmioFOURCC('f', 'm', 't', ' ');
	if (mmioDescend(hmmio, &mmckinfoSubchunk, &mmckinfoParent, MMIO_FINDCHUNK))
	{
		LOG_ERROR("Failed to find the format chunk.");
		mmioClose(hmmio, 0);
		return 0;
	}
	
	// Get the size of the format chunk, allocate memory for it 
	DWORD dwFmtSize = mmckinfoSubchunk.cksize;
	WAVEFORMATEX * lpWaveFormat = (WAVEFORMATEX *) calloc (1, dwFmtSize);
	if (!lpWaveFormat)
	{
		LOG_ERROR("Failed on lpWaveFormat");
		mmioClose(hmmio, 0);
		return 0;
	}
	
	// Read the format chunk 
	if (mmioRead(hmmio, (LPSTR) lpWaveFormat, dwFmtSize) != (LONG) dwFmtSize)
	{
		LOG_ERROR("Failed to read the format chunk.");
		free(lpWaveFormat);
		mmioClose(hmmio, 0);
		return 0;
	}
	
	// Ascend out of the format subchunk 
	mmioAscend(hmmio, &mmckinfoSubchunk, 0);
	
	// Find the data subchunk 
	mmckinfoSubchunk.ckid = mmioFOURCC('d', 'a', 't', 'a');
	if (mmioDescend(hmmio, &mmckinfoSubchunk, &mmckinfoParent, MMIO_FINDCHUNK))
	{
		LOG_ERROR("Failed to find the data chunk.");
		free(lpWaveFormat);
		mmioClose(hmmio, 0);
		return 0;
	}
	
	// Get the size of the data subchunk 
	DWORD dwDataSize = mmckinfoSubchunk.cksize;
	if (dwDataSize == 0L)
	{
		LOG_ERROR("Failed on dwDataSize");
		free(lpWaveFormat);
		mmioClose(hmmio, 0);
		return 0;
	}
	    
	// Allocate and lock memory for the waveform data. 
	HPSTR lpWaveData = (LPSTR) calloc(1, dwDataSize );
	if (!lpWaveData)
	{
		LOG_ERROR("Failed on lpWaveData");
		free(lpWaveFormat);
		mmioClose(hmmio, 0);
		return 0;
	}
	
	// Read the waveform data subchunk 
	if (mmioRead(hmmio, (HPSTR) lpWaveData, dwDataSize) != (LONG) dwDataSize)
	{
		LOG_ERROR("Failed on mmioRead2");
		free(lpWaveFormat);
		free(lpWaveData);
		mmioClose(hmmio, 0);
		return 0;
	}
	
	// We're done with the file, close it 
	mmioClose(hmmio, 0);
	
	LOG_DEBUG("Closed the file");
	
	// Allocate a waveform data header 
	WAVEHDR * lpWaveHdr = (WAVEHDR *) calloc (1,(DWORD)sizeof(WAVEHDR));
	if (!lpWaveHdr)
	{
		LOG_ERROR("Failed on Allocate a waveform data header");
		free(lpWaveData);
		free(lpWaveFormat);
		return 0;
	}
	
	// Set up WAVEHDR structure and prepare it to be written to wave device 
	lpWaveHdr->lpData = lpWaveData;
	lpWaveHdr->dwBufferLength = dwDataSize;
	lpWaveHdr->dwFlags = 0L;
	lpWaveHdr->dwLoops = 0L;
	lpWaveHdr->dwUser  = dw_devid;
	
	// make sure wave device can play our format 
	if (waveOutOpen((LPHWAVEOUT)nullptr, (WORD)dw_devid, (LPWAVEFORMATEX)lpWaveFormat, 0L, 0L,
			WAVE_FORMAT_QUERY | WAVE_MAPPED))
	{
		free(lpWaveFormat);
		free(lpWaveData);
		free(lpWaveHdr);
		LOG_ERROR("Unsupported wave format.");
		return 0;
	}
	
	HWAVEOUT hWave;
	// open the wave device corresponding to the line 
	if (long lErr = waveOutOpen (&hWave, (WORD) dw_devid, (LPWAVEFORMATEX) lpWaveFormat,
			(DWORD) waveOutProc, 0L, CALLBACK_FUNCTION | WAVE_MAPPED))
	{
		free(lpWaveFormat);
		free(lpWaveData);
		free(lpWaveHdr);
		
		switch(lErr)
		{
			case MMSYSERR_ALLOCATED:
				LOG_ERROR("MMSYSERR_ALLOCATED");
				break;
				case MMSYSERR_BADDEVICEID:
				LOG_ERROR("MMSYSERR_BADDEVICEID");
				break;
			case MMSYSERR_NODRIVER:
				LOG_ERROR("MMSYSERR_NODRIVER");
				break;
			case MMSYSERR_NOMEM:
				LOG_ERROR("MMSYSERR_NOMEM");
				break;
			case WAVERR_BADFORMAT:
				LOG_ERROR("WAVERR_BADFORMAT");
				break;
			case WAVERR_SYNC:
				LOG_ERROR("WAVERR_SYNC");
				break;
			default:
				LOG_ERROR("Error not defined");
		}
		LOG_ERROR("Error opening wave device.");
		LOG_ERROR("lErr is %d device id -> %d", lErr, dw_devid);
		return 0;
	}
	
	free(lpWaveFormat);
	
	// prepare the message header for playing
	if (waveOutPrepareHeader (hWave, lpWaveHdr, sizeof(WAVEHDR)))
	{
		LOG_ERROR("Error preparing message header.");
		free(lpWaveData);
		free(lpWaveHdr);
		return 0;
	}
	
	// play the message right from the data segment;  set the play message flag
	if (waveOutWrite (hWave, lpWaveHdr, sizeof (WAVEHDR)))
	{
		LOG_ERROR("Error writing wave message.");
		free(lpWaveData);
		free(lpWaveHdr);
		return 0;
	}
	
	m_devices[dw_devid + 1].b_playData = false;
	
	while (m_devices[dw_devid + 1].b_playData == false)
	{
		Sleep(20);
	}
	
	MMRESULT  res = waveOutReset(hWave);
	if (res != MMSYSERR_NOERROR)
	{
		LOG_ERROR("Error from waveOutReset");
	}
	res = waveOutClose(hWave);
	
	if (res != MMSYSERR_NOERROR)
	{
		LOG_ERROR("Error from waveOutClose");
	}
	
	free(lpWaveData); // free the wave data 
	free(lpWaveHdr); // free the header 
	
	LOG_DEBUG("returning OK");
	
	return hWave;
}

/**
 * Stop playing sound by stopping to play a file or streaming to the speakers.
 * 
 * @param JNIEnv pointer to the JNI Environment
 * @param oObj the Java object that called down the JNI layer
 * @param lHandle the line handle
 * @return
 */
JNIEXPORT jint JNICALL Java_net_xtapi_serviceProvider_MSTAPI_stopPlaying(JNIEnv * pEnv, jobject oObj, jint lHandle)
{
	// The routines to play / record always check a data flag and a done flag
	// so we simply set them to true to end the loop.

	long id;
	if (lHandle != -1)
	{
		id = getWaveID((HLINE) lHandle, waveIn);
	}
	else // this should never happen
	{
		LOG_WARN("Unexpected line handle -1");
		id = -1;
	}

	/*
	 * If streaming to the speakers, sound is recorded from the line and and played out of the speakers.
	 * So both streams have to be stopped.
	 * If b_useDefault is true, then sound is streamed to the speakers.
	 */
	if (m_devices[id + 1].b_useDefault)
	{
		m_devices[0].b_playData = true;
		m_devices[0].b_recDone = true;
		LOG_INFO("shutting down default");
	}
	m_devices[id + 1].b_playData = true;
	m_devices[id + 1].b_recDone = true;

	LOG_INFO("stopping wave device %d", id);
	// If another thread is playing/recording, give it a chance to check the flags
	while (m_devices[id + 1].b_play_Rec)
	{
		Sleep(40);
	}
	Sleep(100);

	return 0;
}

/**
 * Writes a chunk of wave data either to a file or straight to a sound device depending on whether
 * @p fptr or hWaveOut are NULL. Only one of those should be NULL.
 * 
 * @param hdr
 * @param fptr the file to write to - may be NULL
 * @param hWaveOut the device from which to play the sound - may be NULL
 * @param dev_id the ID of the device
 */
void writeRecData(WAVEHDR * hdr, FILE * fptr, HWAVEOUT hWaveOut, DWORD dev_id)
{
	if (nullptr == fptr)
	{
		// We are rendering directly to the speakers or from the mic out the
		// TAPI line, not saving to a file so 
		// setup a WAVEHDR.struct.  All we need is a pointer to the data and
		// the size of that data which we can get of the WAVEHDR passed in.
		WAVEHDR * lpWaveHdr = (WAVEHDR *) calloc(1, (DWORD) sizeof(WAVEHDR));

		lpWaveHdr->lpData = hdr->lpData;
		lpWaveHdr->dwBufferLength = hdr->dwBytesRecorded;
		lpWaveHdr->dwFlags = 0L;
		lpWaveHdr->dwLoops = 0L;
		//lpWaveHdr->dwUser  = (long)lpWaveHdr;	// TODO: Uneeded
		lpWaveHdr->dwUser  = dev_id;

		// Prepare the header
		if (waveOutPrepareHeader (hWaveOut, lpWaveHdr, sizeof(WAVEHDR)))
		{
			LOG_ERROR("Error preparing message header.");
		}

		// Render it
		// Once written to the device we will get a WIM_DATA message back when 
		// this segment is done playing.  There we set the m_devices[id] variable 
		// playHeader in the call back procedure waveOutProc which also sets
		// the flag bPlayData signaling us to free the memory we just allocated 
		// for the header.  We free it in the loop we called this procedure from.
		if (waveOutWrite (hWaveOut, lpWaveHdr, sizeof (WAVEHDR)))
		{
			LOG_ERROR("waveOutWrite error.");
		}
	}
	else
	{
		fwrite(hdr->lpData,hdr->dwBytesRecorded,1,fptr);
	}
}

/**
 * Streams audio to either the given file or device.
 * Uses a circular buffer for unlimited recording/rendering time
 * 
 * @param dw_deviceid ID of the device
 * @param filename the name of the file to stream to
 * @param hWaveOut the handle of the open device to which to stream to
 * @param out_id
 * @return
 */
HWAVEIN recordMessage(DWORD dw_devid, const char * filename, HWAVEOUT hWaveOut, DWORD out_id)
{
	char szTempFileName[MAX_PATH + 1]; // File name buffer
	FILE * fptr = nullptr; // File pointer
	if (filename != nullptr)
	{
		// We are recording to a file.  We stream to a temp file then
		// write the data to a standard wave file when we are done.

		// Windows will give us a unique file name.
		GetTempFileName(".", "XTP", 1, szTempFileName);
		fptr = fopen(szTempFileName,"w+b");
	}

	// Open the wave device for recording
	// The default WAVE_MAPPER device dislikes the WAVE_MAPPED flag.
	// Modems generally don't work without the WAVE_MAPPED flag.
	LONG lrc;
	unsigned long ulFlags = (dw_devid == -1) ? CALLBACK_FUNCTION : (CALLBACK_FUNCTION | WAVE_MAPPED);
	HWAVEIN hWavein; // Handle to wave in device
	if (lrc = waveInOpen(&hWavein, dw_devid, &messageFormat, (DWORD)waveInProc, 0, ulFlags))
	{
		LOG_ERROR("Error opening wave record device.");
		
		switch (lrc)
		{
			case MMSYSERR_ALLOCATED:
				LOG_ERROR("MMSYSERR_ALLOCATED");
				break;
			case MMSYSERR_BADDEVICEID:
				LOG_ERROR("MMSYSERR_BADDEVICEID");
				break;
			case MMSYSERR_NODRIVER:
				LOG_ERROR("MMSYSERR_NODRIVER");
				break;
			case MMSYSERR_NOMEM:
				LOG_ERROR("MMSYSERR_NOMEM");
				break;
			case WAVERR_BADFORMAT:
				LOG_ERROR("WAVERR_BADFORMAT");
				break;
			case WAVERR_SYNC:
				LOG_ERROR("WAVERR_SYNC");
				break;
			default:
				LOG_ERROR("Error not defined");
		}
		return 0;
	}

	// We successfully opened the wave device!
	LOG_DEBUG("Opened device");

	// Setup our circular buffer.  We will add 1/2 of the buffers
	// to the wave device, the others get added on as needed.
	int iDiv = getDivisor();
	WAVEHDR t_inMessageHeader[NUM_BUFFERS]; // Circular buffer
	
	for (int iLoop = 0; iLoop < NUM_BUFFERS / 2; iLoop++)
	{
		memset(&t_inMessageHeader[iLoop],0,sizeof(WAVEHDR));
		waveAlloc(&hWavein, &t_inMessageHeader[iLoop], iDiv);
		t_inMessageHeader[iLoop].dwUser = dw_devid;
		waveInAddBuffer(hWavein, &t_inMessageHeader[iLoop], sizeof(WAVEHDR));
	}

	// We still still to prepare the headers we didn't add to
	// the wave device.
	for (int iLoop = NUM_BUFFERS / 2 ; iLoop < NUM_BUFFERS; iLoop++)
	{
		memset(&t_inMessageHeader[iLoop],0,sizeof(WAVEHDR));
		waveAlloc(&hWavein, &t_inMessageHeader[iLoop], iDiv);
		t_inMessageHeader[iLoop].dwUser = dw_devid;
	}

	// Start recording
	waveInStart(hWavein);

	m_devices[dw_devid + 1].b_recDone = false;
	m_devices[dw_devid + 1].b_recData = false;
	unsigned long numLoops = NUM_BUFFERS / 2;	// Start in the middle of the array

	/*
	 * We cyle through an array of pre-allocated wave headers.
	 * With the requirement to use JTapi as a speakerphone we
	 * need small buffers (1000ms) so we don't have a large delay
	 * however we need enough buffer time so that we can be 
	 * interrupted by the system without choking so we use
	 * NUM_BUFFERS which is as I write this is 20 (subject to change..)
	 * and upfront we feed in half the array to get us started then as
	 * buffers are used we continue to add them in a circular fashion.
	 */
	unsigned long ulBytesRecorded = 0; // Number of bytes recorded
	while (m_devices[dw_devid + 1].b_recDone == false)
	{
		if (m_devices[out_id + 1].b_playData == true)
		{
			if (filename == nullptr)
			{
				// We are rendering to the speakers as opposed to writing to a file
				// so we need to clean up the wave headers as we go.
				
				long lRet = waveOutUnprepareHeader(hWaveOut,
								m_devices[out_id + 1].playHeader,
								sizeof(WAVEHDR));
				if (lRet != 0)
				{
					switch (lRet)
					{
						case MMSYSERR_INVALHANDLE:
							LOG_ERROR("waveOutUnprepareHeader MMSYSERR_INVALHANDLE");
							break;
						case MMSYSERR_NODRIVER:
							LOG_ERROR("waveOutUnprepareHeader MMSYSERR_NODRIVER");
							break;
						case MMSYSERR_NOMEM:
							LOG_ERROR("waveOutUnprepareHeader MMSYSERR_NOMEM");
							break;
						case WAVERR_STILLPLAYING:
							LOG_ERROR("waveOutUnprepareHeader WAVERR_STILLPLAYING");
							break;
						default:
							LOG_ERROR("waveOutUnprepareHeader error %d", lRet);
							break;
					}
				}
				else
				{
					// We didn't get an error during waveOutUnprepareHeader 
					// so assume it is safe to free the header.  
					// NOTE:  
					// Just frees header, does not free data, we always 
					// reuse the allocated data space and only free it at the
					// end of this procedure.
					free(m_devices[out_id + 1].playHeader);
				}

				m_devices[out_id + 1].b_playData = false;
			}
		}

		/**
		 *  We have data so we need to save the data chunk and allow this piece
		 *  of allocated memory to be flagged for re-use.
		 */
		if (m_devices[dw_devid + 1].b_recData)
		{
			ulBytesRecorded += m_devices[dw_devid + 1].recHeader->dwBytesRecorded;

			writeRecData(m_devices[dw_devid + 1].recHeader,fptr, hWaveOut,out_id);

			long lRc = waveInAddBuffer (hWavein, 
									&t_inMessageHeader[numLoops%NUM_BUFFERS],
									sizeof(WAVEHDR));
			if (lRc != 0)
			{
				switch (lRc)
				{
					case MMSYSERR_INVALHANDLE:
						LOG_ERROR("waveInAddBuffer MMSYSERR_INVALHANDLE");
						break;
					case MMSYSERR_NODRIVER:
						LOG_ERROR("waveInAddBuffer MMSYSERR_NODRIVER");
						break;
					case MMSYSERR_NOMEM:
						LOG_ERROR("waveInAddBuffer MMSYSERR_NOMEM");
						break;
					case WAVERR_UNPREPARED:
						LOG_ERROR("waveInAddBuffer WAVERR_UNPREPARED");
						break;
					default:
						LOG_ERROR("waveInAddBuffer = %d\n", lRc);
						break;
				}
				
			}

			// increment loop counter to track header array for reuse.
			numLoops++;	
			m_devices[dw_devid + 1].b_recData = false;

		}
		else
		{
			// We don't have anything to do so don't use 100% of the CPU
			// servicing this loop.
			// Sleep(0) SHOULD give up our current time slice however on
			// a multi-proc system we still use 100% of the CPU so we sleep
			// for an arbritrary amount of time.
			Sleep(20);
		}
	}

	if (fptr != nullptr)
	{
		// We aren't rendering to the speakers or a TAPI line so save the 
		// temp file to a standard wave file.
		fflush(fptr);
		saveMessage((char*)filename, m_devices[dw_devid + 1].recHeader, 
					ulBytesRecorded, fptr);
		fclose(fptr);

		// Delete the temp file.
		DeleteFile(szTempFileName);
	}

	// Reset wave device 
	MMRESULT res = waveInReset( hWavein );
	if (res != MMSYSERR_NOERROR)
	{
		LOG_ERROR("waveInReset Error -> %d\n",res);
		return hWavein;
	}

	// Cleanup the header array
	for (int iLoop = 0; iLoop < NUM_BUFFERS; iLoop++)
	{
		waveInUnprepareHeader (hWavein, &t_inMessageHeader[iLoop], sizeof(WAVEHDR));
		GlobalFree (t_inMessageHeader[iLoop].lpData); //free the data buffer 
	}
	
	// Close wave device 
	waveInClose (hWavein); 
	hWavein = nullptr;

	return hWavein;
}

JNIEXPORT jint JNICALL Java_net_xtapi_serviceProvider_MSTAPI_stopRecording(JNIEnv * pEnv, jobject oObj, jint lHandle)
{
	long id = (lHandle != -1) ? getWaveID((HLINE) lHandle, waveIn) : -1;
	if (m_devices[id + 1].b_useDefault)
	{
		m_devices[0].b_playData = true;
		m_devices[0].b_recDone = true;
		LOG_INFO("Shutting down default");
	}

	LOG_INFO("stopping wave device %d", id);
	m_devices[id + 1].b_recDone = true;

	// If another thread is playing/recording give it a chance to check
	// the flags
	while (m_devices[id + 1].b_play_Rec)
	{
		Sleep(40);
	}
	Sleep(100);

	return 0;
}

/**
 * Allocates and prepares a wave header.
 * 
 * @param h_wavein
 * @param waveHeader
 * @param divisor
 * @return
 */
long waveAlloc(HWAVEIN * h_wavein, WAVEHDR * waveHeader, int divisor)
{
	// Allocate the message buffer
	// The smaller dw_bufsz the less latency for a speaker phone.
	// On a Dialogic 160SC the smallest I could set is 1 second, lower than that and sound was lost!
	DWORD dw_bufsz = (1L * (long) messageFormat.nSamplesPerSec * (long) messageFormat.wBitsPerSample/8L) / divisor;
	HANDLE hData = GlobalAlloc (GPTR, dw_bufsz);
	if ((waveHeader->lpData = (char *)GlobalLock(hData)) == nullptr)
	{
		LOG_ERROR("Not enough memory.");
		return 0;
	}

	memset(hData, 0, dw_bufsz);
	waveHeader->dwBufferLength = dw_bufsz;

	long lrc;
	if (lrc = waveInPrepareHeader(*h_wavein, waveHeader, sizeof(WAVEHDR)))
	{
		GlobalFree(hData);
		LOG_ERROR("Error preparing message header.");
		return 0;
	}

	waveHeader->dwUser = (DWORD) hData;
	
	return 0;
}

/**
 * Saves the message after the caller hangs up or after 60s as a recorded voice message
 * 
 * @param name
 * @param hdr
 * @param ulBytes
 * @param fptr
 * @return
 */
int saveMessage (char *name, LPWAVEHDR hdr, unsigned long ulBytes, FILE * fptr)
{
	// WAVE header constants
	BYTE ab_riffchunk[] = {'R','I','F','F',  0,0,0,0, 'W','A','V','E'}; // RIFF chunk
	BYTE ab_formatchunktag[] = {'f','m','t',' ',  0,0,0,0}; // Format chunk tag
	BYTE ab_datachunktag[] = {'d','a','t','a',  0,0,0,0}; // Data chunk header
	
	HANDLE h_file = CreateFile (name, GENERIC_READ|GENERIC_WRITE, 0, nullptr, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, nullptr);
	if (h_file == nullptr)
	{
		LOG_ERROR(strerror(errno));
		return -1;
	}
	
	// write out the RIFF chunk 
	*((DWORD *)&ab_riffchunk[4]) = 4 + sizeof(ab_formatchunktag) + sizeof(messageFormat)
			+ sizeof(ab_datachunktag) + ulBytes;
	unsigned long n_written;
	WriteFile(h_file, ab_riffchunk, sizeof(ab_riffchunk), &n_written, nullptr);
	*((DWORD *)&ab_formatchunktag[4]) = sizeof(messageFormat); /* write  tag */
	WriteFile(h_file, ab_formatchunktag, sizeof(ab_formatchunktag), &n_written, nullptr);
	
	// write out the canned format header 
	WriteFile (h_file, &messageFormat, sizeof(messageFormat), &n_written, nullptr);
	
	// write out the data chunk tag 
	*((DWORD *)&ab_datachunktag[4]) = ulBytes;
	WriteFile (h_file, ab_datachunktag, sizeof(ab_datachunktag), &n_written, nullptr);
	
	// write out the data chunk 
	// Seek to the beginning of the temp file.
	int result = fseek(fptr, 0, SEEK_SET);
	if (result)
	{
		LOG_ERROR(strerror(errno));
		return 1;
	}
	
	// Use a 100 kB buffer to copy the raw data to the wave file data section
	const unsigned long buffsize = 100 * 1024;
	LPSTR buff = (LPSTR) malloc(buffsize);
	while (true)
	{
		size_t iRead = fread(buff, 1, buffsize, fptr);
		if (iRead > 0)
		{
			WriteFile(h_file, buff, iRead, &n_written, nullptr);
		}
		else
		{
			break;
		}
	}
	free(buff);
	CloseHandle (h_file); // close message file 
	return 0;
}

/**
 * Opens a TAPI wave device.
 * Should be used if rendering to a device such as the speakers or using the microphone and
 * streaming out to the TAPI line.
 * 
 * NOTE: This should probably be called from #playFile()
 * 
 * @param device the device to be opened
 * @return the opened device's handle
 */
HWAVEOUT openPlayDevice(DWORD device)
{
	// open the wave device
	long lErr;
	HWAVEOUT hWaveOut;
	unsigned long ulFlags = (device == -1) ? CALLBACK_FUNCTION : (CALLBACK_FUNCTION | WAVE_MAPPED);
	if (lErr = waveOutOpen (&hWaveOut, device, (LPWAVEFORMATEX) &messageFormat, (DWORD) waveOutProc, 0L, ulFlags))
	{
		switch(lErr)
		{
			case MMSYSERR_ALLOCATED:
				LOG_ERROR("MMSYSERR_ALLOCATED");
				break;
			case MMSYSERR_BADDEVICEID:
				LOG_ERROR("MMSYSERR_BADDEVICEID");
				break;
			case MMSYSERR_NODRIVER:
				LOG_ERROR("MMSYSERR_NODRIVER");
				break;
			case MMSYSERR_NOMEM:
				LOG_ERROR("MMSYSERR_NOMEM");
				break;
			case WAVERR_BADFORMAT:
				LOG_ERROR("WAVERR_BADFORMAT");
				break;
			case WAVERR_SYNC:
				LOG_ERROR("WAVERR_SYNC");
				break;
			default:
				LOG_ERROR("failed to open output wave device % d\n", lErr);
		}

		return nullptr;
	}

	return hWaveOut;
}

/*
 * Reads the value of the registry value at @c HKEY_LOCAL_MACHINE\SOFTWARE\xtapi with the name
 * @c divisor.
 * When streaming audio (using getDefaultXXXX) it is preferable to have smaller buffers. However,
 * buffers smaller than 1 second do not work correctly on some cards (e. g. Dialogic 160SC). The
 * default buffer size is one second.
 * A custom divisor can be set in the Windows registry at @c HKEY_LOCAL_MACHINE\SOFTWARE\xtapi to
 * divide one second by this divisor. The name has to be @c divisor. The value has to be a @c DWORD
 * in the interval [1, 10].
 * E. g. setting the value to 5 results in 200 ms buffers.
 * 
 * @return the read value or 1 if the value could not be read
 */
int getDivisor()
{
	DWORD dwValue = 1;
	HKEY hKey;
	long lRes = RegOpenKeyEx(HKEY_LOCAL_MACHINE, // handle to open key
			"SOFTWARE\\xtapi", // subkey name
			0, // reserved
			KEY_QUERY_VALUE, // security access mask
			&hKey); // handle to open key
	if (lRes == ERROR_SUCCESS)
	{
		unsigned long ulType = REG_DWORD;
		unsigned char szValue[10];
		DWORD dwSize = 10;
		lRes = RegQueryValueEx(hKey, "divisor", nullptr, &ulType, szValue, &dwSize);
		if (lRes == ERROR_SUCCESS)
		{
			dwValue = (DWORD) szValue;
			if (dwValue < 1)
			{
				dwValue = 1;
			}
			else if (dwValue > 10)
			{
				dwValue = 10;
			}
		}

		RegCloseKey(hKey);
	}

	return dwValue;
}
