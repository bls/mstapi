 /**
 * Logging utilities
 *
 *  Copyright (C) 2021 Fabian Hall - f.hall (at) bls-integration.de
 * 
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * @author Fabian Hall - f.hall (at) bls-integration.de
 * @version 1.2.0
 */
#ifndef TAPI_LOG_HEADER
#define TAPI_LOG_HEADER

#include <windows.h>

typedef struct
{
	char * name;
	unsigned char value;
} Severity;

typedef struct
{
	const Severity fatal;
	const Severity error;
	const Severity warn;
	const Severity info;
	const Severity debug;
	const Severity trace;
	const int numLevels;
	const Severity levels[6];
} LogLevel;

const LogLevel LOG_LEVEL =
{
	{"FATAL", 0},
	{"ERROR", 1},
	{"WARN", 2},
	{"INFO", 4},
	{"DEBUG", 8},
	{"TRACE", 16},
	6,
	{LOG_LEVEL.fatal, LOG_LEVEL.error, LOG_LEVEL.warn, LOG_LEVEL.info, LOG_LEVEL.debug, LOG_LEVEL.trace}
};

// Acknowledgement: _where() macro from http://gcc.gnu.org
#define _wh_(_file, _line)  _file ":" #_line
#define __wh_(file, line) _wh_(file, line)
#define _where() __wh_(__FILE__, __LINE__)
#define LOG(_severity, _messageFormat, ...)\
{\
	if (_severity.value <= getMinSeverity().value)\
	{\
		char _szLogMessage[256];\
		wsprintf(_szLogMessage, _messageFormat, ##__VA_ARGS__);\
		logMessage(_severity, __FUNCTION__, _szLogMessage, _where());\
	}\
}
#define LOG_FATAL(_messageFormat, ...) LOG(LOG_LEVEL.fatal, _messageFormat, ##__VA_ARGS__)
#define LOG_ERROR(_messageFormat, ...) LOG(LOG_LEVEL.error, _messageFormat, ##__VA_ARGS__)
#define LOG_WARN(_messageFormat, ...) LOG(LOG_LEVEL.warn, _messageFormat, ##__VA_ARGS__)
#define LOG_INFO(_messageFormat, ...) LOG(LOG_LEVEL.info, _messageFormat, ##__VA_ARGS__)
#define LOG_DEBUG(_messageFormat, ...) LOG(LOG_LEVEL.debug, _messageFormat, ##__VA_ARGS__)
#define LOG_TRACE(_messageFormat, ...) LOG(LOG_LEVEL.trace, _messageFormat, ##__VA_ARGS__)
#define LOG_ERROR_ON_ERROR_CODE(_errorCode, _messageFormat, ...)\
{\
	Severity _severity = (_errorCode == 0) ? LOG_LEVEL.debug : LOG_LEVEL.error;\
	LOG(_severity, _messageFormat, ##__VA_ARGS__);\
}
#define LOG_WARN_ON_ERROR_CODE(_errorCode, _messageFormat, ...)\
{\
	Severity _severity = (_errorCode == 0) ? LOG_LEVEL.debug : LOG_LEVEL.warn;\
	LOG(_severity, _messageFormat, ##__VA_ARGS__);\
}

/*
 * Logs a message
 * 
 * @param severity severity of the log message
 * @param functionName name of the function which requested the log entry
 * @param logMsg log message
 * @param location <absolute file name>:<line number of the function call>
 */
void logMessage(Severity severity, const char * functionName, const char * logMsg, const char * location);

/**
 * Opens and initializes the log
 */
void openLog();

/**
 * Closes the log
 */
void closeLog();

/**
 * Returns the minimum severity for a log message to be written to a log file
 * 
 * @return the minimum severity
 */
Severity getMinSeverity();

/**
 * Sets the minimum severity for a log message to be written to a log file
 * 
 * @param severity the new minimum severity
 */
void setMinSeverity(Severity severity);

#endif // TAPI_LOG_HEADER
