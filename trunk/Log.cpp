/**
*  Logger implementation
*  Copyright (C) 2021 Fabian Hall - f.hall (at) bls-integration.de
* 
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*  
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*  
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
* @author Fabian Hall - f.hall (at) bls-integration.de
*/
#include "Log.h"
#include <string>
#include <sys/timeb.h> // ANSI Standard
#include <time.h>

/**
 * Returns the log file's current size
 */
long getLogSize();
/**
 * Archives the current log and starts a new one
 */
void rollOverLog();

/**
 * Log file pointer
 */
FILE * fptr;
/**
 * Log file name
 */
const char * LOG_FILE_NAME = "mstapi.log";
/**
 * Archived log file name
 */
const char * LOG_ARCHIVE_FILE_NAME = "mstapi_1.log";
/**
 * Maximum log file size
 */
const long MAX_LOG_SIZE_BYTES = 25000000L;  // 25 MB
/**
 * Lowest log level to write to the log file
 */
Severity minSeverity = LOG_LEVEL.trace;

void openLog()
{
	fptr = fopen(LOG_FILE_NAME, "a"); // append existing log file or create it if non-existent
	if (NULL != fptr)
	{
		setvbuf(fptr, NULL, _IONBF, BUFSIZ); // do not buffer any output

		struct _timeb timebuffer;
		char * timeline;
		_ftime(&timebuffer);
		timeline = ctime(&(timebuffer.time));

		// put a header on the log entry:
		char * annoy = "-----------";
		fprintf(fptr, "\n %s Logging Started %.19s.%-3hu %s \n", annoy, timeline, timebuffer.millitm, annoy);
		fprintf(fptr, "Timestamp               Level [Function(Location)]         Message\n");
	}
}

void closeLog()
{
	fclose(fptr);
}

Severity getMinSeverity()
{
	return minSeverity;
}

void setMinSeverity(Severity severity)
{
	minSeverity = severity;
}

void logMessage(Severity severity, const char * functionName, const char * logMsg, const char * location)
{
	if (fptr == NULL || severity.value > minSeverity.value)
	{
		return;
	}

	// Trim off the file path
	const char * str = strrchr(location, '\\');

	if (NULL != str)
	{
		location = str + 1;
	}

	struct _timeb timebuffer; // time struct
	char * pchTimeline = NULL; // time string
	_ftime(&timebuffer);
	pchTimeline = ctime(&(timebuffer.time));
	if (NULL != pchTimeline)
	{
		if (getLogSize() > MAX_LOG_SIZE_BYTES)
		{
			rollOverLog();
		}
		fprintf(fptr, "%.19s.%-3u %-5s [%s(%s)] %s\n",
				pchTimeline, timebuffer.millitm, severity.name, functionName, location, logMsg);
	}
}

long getLogSize()
{
	if (fptr == NULL)
	{
		return 0;
	}

	fseek(fptr, 0, SEEK_END);
	long logSize = ftell(fptr);
	fseek(fptr, 0, SEEK_SET);
	return logSize;
}

void rollOverLog()
{
	if (fptr == NULL)
	{
		return;
	}

	fclose(fptr);
	remove(LOG_ARCHIVE_FILE_NAME);
	rename(LOG_FILE_NAME, LOG_ARCHIVE_FILE_NAME);
	fptr = fopen(LOG_FILE_NAME, "a"); // append existing log file or create it if non-existent
}
